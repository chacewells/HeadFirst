package com.wellsfromwales.hfdesignpatterns.command;

import static com.wellsfromwales.hfdesignpatterns.command.Devices.*;

public class Commands {

	public static class GarageDoorUpCommand implements Command {
		GarageDoor garageDoor;

		public GarageDoorUpCommand(GarageDoor garageDoor) {
			this.garageDoor = garageDoor;
		}

		@Override
		public void execute() {
			System.out.println("Garage Door is Open");
		}

		@Override
		public void undo() {
			garageDoor.down();
		}

	}
	public static class GarageDoorDownCommand implements Command {
		GarageDoor garageDoor;
		
		public GarageDoorDownCommand(GarageDoor garageDoor) {
			this.garageDoor = garageDoor;
		}

		@Override
		public void execute() {
			System.out.println("Garage Door is Closed");
		}

		@Override
		public void undo() {
			garageDoor.up();
		}
	}
	public static class GarageDoorStopCommand implements Command {
		GarageDoor garageDoor;
		
		public GarageDoorStopCommand(GarageDoor garageDoor) {
			this.garageDoor = garageDoor;
		}

		@Override
		public void execute() {
			System.out.println("Garage Door Stopped");
		}

		@Override
		public void undo() {
		}
	}
	public static class GarageDoorLightOnCommand implements Command {
		GarageDoor garageDoor;
		
		public GarageDoorLightOnCommand(GarageDoor garageDoor) {
			this.garageDoor = garageDoor;
		}

		@Override
		public void execute() {
			System.out.println("Garage door light is on");
		}

		@Override
		public void undo() {
			garageDoor.lightOff();
		}
	}
	public static class GarageDoorLightOffCommand implements Command {
		GarageDoor garageDoor;
		
		public GarageDoorLightOffCommand(GarageDoor garageDoor) {
			this.garageDoor = garageDoor;
		}

		@Override
		public void execute() {
			System.out.println("Garage door light is off");
		}

		@Override
		public void undo() {
			garageDoor.lightOn();
		}
	}
	public static class LightOffCommand implements Command {
		Light light;
		
		public LightOffCommand(Light light) {
			this.light = light;
		}

		@Override
		public void execute() {
			light.off();
		}

		@Override
		public void undo() {
			light.on();
		}

	}
	public static class LightOnCommand implements Command {
		Light light;
		
		public LightOnCommand( Light light ) {
			this.light = light;
		}

		@Override
		public void execute() {
			light.on();
		}

		@Override
		public void undo() {
			light.off();
		}

	}
	public static class ApplianceControlOnCommand implements Command {
		ApplianceControl applianceControl;
		
		public ApplianceControlOnCommand(ApplianceControl applianceControl) {
			this.applianceControl = applianceControl;
		}

		@Override
		public void execute() {
			applianceControl.on();
		}

		@Override
		public void undo() {
			applianceControl.off();
		}
	}
	public static class ApplianceControlOffCommand implements Command {
		ApplianceControl applianceControl;
		
		public ApplianceControlOffCommand(ApplianceControl applianceControl) {
			this.applianceControl = applianceControl;
		}

		@Override
		public void execute() {
			System.out.println("Appliance is Off");
		}

		@Override
		public void undo() {
			applianceControl.on();
		}
	}
	public static class StereoOnCommand implements Command {
		Stereo stereo;
		
		public StereoOnCommand(Stereo stereo) {
			this.stereo = stereo;
		}

		@Override
		public void execute() {
			stereo.on();
		}

		@Override
		public void undo() {
			stereo.off();
		}

	}
	public static class StereoOffCommand implements Command {
		Stereo stereo;
		
		public StereoOffCommand(Stereo stereo) {
			this.stereo = stereo;
		}

		@Override
		public void execute() {
			stereo.off();
		}

		@Override
		public void undo() {
			stereo.on();
		}

	}
	public static class StereoOnWithCdCommand implements Command {
		Stereo stereo;
		
		public StereoOnWithCdCommand(Stereo stereo) {
			this.stereo = stereo;
		}

		@Override
		public void execute() {
			stereo.on();
			stereo.setCd();
			stereo.setVolume(11);
		}

		@Override
		public void undo() {
			stereo.off();
		}

	}
	public static class StereoOnWithDvdCommand implements Command {
		Stereo stereo;
		
		public StereoOnWithDvdCommand(Stereo stereo) {
			this.stereo = stereo;
		}

		@Override
		public void execute() {
			stereo.on();
			stereo.setDvd();
			stereo.setVolume(11);
		}

		@Override
		public void undo() {
			stereo.off();
		}

	}
	public static class StereoOnWithRadioCommand implements Command {
		Stereo stereo;
		
		public StereoOnWithRadioCommand(Stereo stereo) {
			this.stereo = stereo;
		}

		@Override
		public void execute() {
			stereo.on();
			stereo.setRadio();
			stereo.setVolume(11);
		}

		@Override
		public void undo() {
			stereo.off();
		}

	}
	public static class StereoVolumeUpCommand implements Command {
		Stereo stereo;
		
		public StereoVolumeUpCommand(Stereo stereo) {
			this.stereo = stereo;
		}

		@Override
		public void execute() {
			stereo.setVolume(stereo.getVolume() + 1);
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class StereoVolumeDownCommand implements Command {
		Stereo stereo;
		
		public StereoVolumeDownCommand(Stereo stereo) {
			this.stereo = stereo;
		}

		@Override
		public void execute() {
			stereo.setVolume(stereo.getVolume() - 1);
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class TvOnCommand implements Command {
		TV tv;
		
		public TvOnCommand(TV tv) {
			this.tv = tv;
		}

		@Override
		public void execute() {
			tv.on();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class TvOffCommand implements Command {
		TV tv;
		
		public TvOffCommand(TV tv) {
			this.tv = tv;
		}

		@Override
		public void execute() {
			tv.off();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class TvChannelUpCommand implements Command {
		TV tv;
		
		public TvChannelUpCommand(TV tv) {
			this.tv = tv;
		}

		@Override
		public void execute() {
			tv.setInputChannel(tv.getInputChannel() + 1);;
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class TvChannelDownCommand implements Command {
		TV tv;
		
		public TvChannelDownCommand(TV tv) {
			this.tv = tv;
		}

		@Override
		public void execute() {
			tv.setInputChannel(tv.getInputChannel() - 1);;
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class TvVolumeUpCommand implements Command {
		TV tv;
		
		public TvVolumeUpCommand(TV tv) {
			this.tv = tv;
		}

		@Override
		public void execute() {
			tv.setVolume(tv.getVolume() + 1);;
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class TvVolumeDownCommand implements Command {
		TV tv;
		
		public TvVolumeDownCommand(TV tv) {
			this.tv = tv;
		}

		@Override
		public void execute() {
			tv.setVolume(tv.getVolume() - 1);;
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class CeilingLightOnCommand implements Command {
		CeilingLight ceilingLight;
		
		public CeilingLightOnCommand(CeilingLight ceilingLight) {
			this.ceilingLight = ceilingLight;
		}

		@Override
		public void execute() {
			ceilingLight.on();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class CeilingLightOffCommand implements Command {
		CeilingLight ceilingLight;
		
		public CeilingLightOffCommand(CeilingLight ceilingLight) {
			this.ceilingLight = ceilingLight;
		}

		@Override
		public void execute() {
			ceilingLight.off();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class CeilingLightDimCommand implements Command {
		CeilingLight ceilingLight;
		
		public CeilingLightDimCommand(CeilingLight ceilingLight) {
			this.ceilingLight = ceilingLight;
		}

		@Override
		public void execute() {
			ceilingLight.dim();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class CeilingFanHighCommand implements Command {
		CeilingFan ceilingFan;
		
		public CeilingFanHighCommand(CeilingFan ceilingFan) {
			this.ceilingFan = ceilingFan;
		}

		@Override
		public void execute() {
			ceilingFan.high();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class CeilingFanMediumCommand implements Command {
		CeilingFan ceilingFan;
		
		public CeilingFanMediumCommand(CeilingFan ceilingFan) {
			this.ceilingFan = ceilingFan;
		}

		@Override
		public void execute() {
			ceilingFan.medium();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class CeilingFanLowCommand implements Command {
		CeilingFan ceilingFan;
		
		public CeilingFanLowCommand(CeilingFan ceilingFan) {
			this.ceilingFan = ceilingFan;
		}

		@Override
		public void execute() {
			ceilingFan.low();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class CeilingFanOffCommand implements Command {
		CeilingFan ceilingFan;
		
		public CeilingFanOffCommand(CeilingFan ceilingFan) {
			this.ceilingFan = ceilingFan;
		}

		@Override
		public void execute() {
			ceilingFan.off();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class SprinklerOnCommand implements Command {
		Sprinkler sprinkler;
		
		public SprinklerOnCommand(Sprinkler sprinkler) {
			this.sprinkler = sprinkler;
		}

		@Override
		public void execute() {
			sprinkler.waterOn();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class SprinklerOffCommand implements Command {
		Sprinkler sprinkler;
		
		public SprinklerOffCommand(Sprinkler sprinkler) {
			this.sprinkler = sprinkler;
		}

		@Override
		public void execute() {
			sprinkler.waterOff();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class OutdoorLightOnCommand implements Command {
		OutdoorLight outdoorLight;
		
		public OutdoorLightOnCommand(OutdoorLight outdoorLight) {
			this.outdoorLight = outdoorLight;
		}

		@Override
		public void execute() {
			outdoorLight.on();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class OutdoorLightOffCommand implements Command {
		OutdoorLight outdoorLight;
		
		public OutdoorLightOffCommand(OutdoorLight outdoorLight) {
			this.outdoorLight = outdoorLight;
		}

		@Override
		public void execute() {
			outdoorLight.off();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class GardenLightManualOnCommand implements Command {
		GardenLight gardenLight;
		
		public GardenLightManualOnCommand(GardenLight gardenLight) {
			this.gardenLight = gardenLight;
		}

		@Override
		public void execute() {
			gardenLight.manualOn();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
	public static class GardenLightManualOffCommand implements Command {
		GardenLight gardenLight;
		
		public GardenLightManualOffCommand(GardenLight gardenLight) {
			this.gardenLight = gardenLight;
		}

		@Override
		public void execute() {
			gardenLight.manualOff();
		}

		@Override
		public void undo() {
			// TODO Auto-generated method stub
			
		}

	}
}
