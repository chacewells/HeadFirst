package com.wellsfromwales.hfdesignpatterns.command;

import com.wellsfromwales.hfdesignpatterns.command.Commands.CeilingFanHighCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.CeilingFanLowCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.CeilingFanMediumCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.CeilingFanOffCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.GarageDoorDownCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.GarageDoorUpCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.LightOffCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.LightOnCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoOffCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoOnWithCdCommand;
import com.wellsfromwales.hfdesignpatterns.command.Devices.CeilingFan;
import com.wellsfromwales.hfdesignpatterns.command.Devices.GarageDoor;
import com.wellsfromwales.hfdesignpatterns.command.Devices.Light;
import com.wellsfromwales.hfdesignpatterns.command.Devices.Stereo;

public class Test {
	
	private static class LabeledLight implements Light {
		
		private String label;
		
		public LabeledLight( String label ) {
			this.label = label;
		}

		@Override
		public void on() {
			System.out.println(label + " Light is now on");
		}

		@Override
		public void off() {
			System.out.println(label + " Light is now off");
		}
		
	}
	
	private static class LabeledCeilingFan implements CeilingFan {
		private String label;
		
		public LabeledCeilingFan(String label) {
			this.label = label;
		}

		@Override
		public void high() {
			System.out.println(label + " Ceiling Fan set to High");
		}

		@Override
		public void medium() {
			System.out.println(label + " Ceiling Fan set to Medium");
		}

		@Override
		public void low() {
			System.out.println(label + " Ceiling Fan set to Low");
		}

		@Override
		public void off() {
			System.out.println(label + " Ceiling Fan is now Off");
		}

		@Override
		public void getSpeed() {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private static GarageDoor garageDoor = new GarageDoor() {
		
		@Override
		public void up() {
			System.out.println("Garage Door is Up");
		}

		@Override
		public void down() {
			System.out.println("Garage Door is Down");
		}

		@Override
		public void stop() {
			System.out.println("Garage Door stopped");
		}

		@Override
		public void lightOn() {
			System.out.println("Garage Door Light is On");
		}

		@Override
		public void lightOff() {
			System.out.println("Garage Door Light is Off");
		}
		
	};
	
	private static class LabeledStereo implements Stereo {
		private String label;
		private int volume;
		
		public LabeledStereo(String label) {
			this.label = label;
		}

		@Override
		public void on() {
			System.out.println(label + " Stereo is On");
		}

		@Override
		public void off() {
			System.out.println(label + " Stereo is Off");
		}

		@Override
		public void setCd() {
			System.out.println(label + " Stereo is set to CD");
		}

		@Override
		public void setDvd() {
			System.out.println(label + " Stereo is set to DVD");
		}

		@Override
		public void setRadio() {
			System.out.println(label + " Stereo is set to Radio");
		}

		@Override
		public void setVolume(int volume) {
			this.volume = volume;
			System.out.println(label + " Stereo Volume is now " + volume);
		}

		@Override
		public int getVolume() {
			return volume;
		}
		
	}
	
	public static void main(String[] args) {
		RemoteControl remoteControl = new RemoteControl();
		
		Light livingRoomLight = new LabeledLight("Living Room");
		Light kitchenLight = new LabeledLight("Kitchen");
		CeilingFan livingRoomFan = new LabeledCeilingFan("Living Room");
		GarageDoor garageDoor = Test.garageDoor;
		Stereo stereo = new LabeledStereo("Living Room");
		
		LightOnCommand livingRoomLightOn = new LightOnCommand(livingRoomLight);
		LightOffCommand livingRoomLightOff = new LightOffCommand(livingRoomLight);
		LightOnCommand kitchenLightOn = new LightOnCommand(kitchenLight);
		LightOffCommand kitchenLightOff = new LightOffCommand(kitchenLight);
		
		CeilingFanHighCommand livingRoomFanHigh = new CeilingFanHighCommand(livingRoomFan);
		CeilingFanOffCommand livingRoomFanOff = new CeilingFanOffCommand(livingRoomFan);
		
		GarageDoorUpCommand garageDoorOpen = new GarageDoorUpCommand(garageDoor);
		GarageDoorDownCommand garageDoorClose = new GarageDoorDownCommand(garageDoor);
		
		StereoOnWithCdCommand stereoOnCd = new StereoOnWithCdCommand(stereo);
		StereoOffCommand stereoOff = new StereoOffCommand(stereo);
		
		remoteControl.setCommand(0, livingRoomLightOn, livingRoomLightOff);
		remoteControl.setCommand(1, kitchenLightOn, kitchenLightOff);
		remoteControl.setCommand(2, livingRoomFanHigh, livingRoomFanOff);
		remoteControl.setCommand(3, stereoOnCd, stereoOff);
		remoteControl.setCommand(4, garageDoorOpen, garageDoorClose);
		
		System.out.println(remoteControl);

		remoteControl.onButtonWasPushed(0);
		remoteControl.offButtonWasPushed(0);
		remoteControl.onButtonWasPushed(1);
		remoteControl.offButtonWasPushed(1);
		remoteControl.onButtonWasPushed(2);
		remoteControl.offButtonWasPushed(2);
		remoteControl.onButtonWasPushed(3);
		remoteControl.offButtonWasPushed(3);
		remoteControl.onButtonWasPushed(4);
		remoteControl.offButtonWasPushed(4);
		
	}
	
}
