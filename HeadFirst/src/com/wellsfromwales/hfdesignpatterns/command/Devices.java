package com.wellsfromwales.hfdesignpatterns.command;

public interface Devices {
	public static interface ApplianceControl {
		public void on();
		public void off();
	}
	public static interface FaucetControl {
		
	}
	public static interface Hottub {
		
	}
	public static interface TV {
		public void on();
		public void off();
		public void setInputChannel(int channel);
		public int getInputChannel();
		public void setVolume(int volume);
		public int getVolume();
	}
	public static interface CeilingLight {
		public void on();
		public void off();
		public void dim();
	}
	public static interface CeilingFan {
		public void high();
		public void medium();
		public void low();
		public void off();
		public void getSpeed();
	}
	public static interface Light {
		public void on();
		public void off();
	}
	public static interface OutdoorLight {
		public void on();
		public void off();
	}
	public static interface GardenLight {
		public void setDuskTime(long time);
		public void setDawnTime(long time);
		public void manualOn();
		public void manualOff();
	}
	public static interface Sprinkler {
		public void waterOn();
		public void waterOff();
	}
	public interface Stereo {
		public void on();
		public void off();
		public void setCd();
		public void setDvd();
		public void setRadio();
		public void setVolume(int volume);
		public int getVolume();
	}
	public static interface GarageDoor {
		public void up();
		public void down();
		public void stop();
		public void lightOn();
		public void lightOff();
	}
}
