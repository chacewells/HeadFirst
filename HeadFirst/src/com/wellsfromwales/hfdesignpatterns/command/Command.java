package com.wellsfromwales.hfdesignpatterns.command;

public interface Command {
	public void execute();
	public void undo();
}
