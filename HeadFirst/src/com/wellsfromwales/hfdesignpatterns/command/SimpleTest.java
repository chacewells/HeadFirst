package com.wellsfromwales.hfdesignpatterns.command;

import com.wellsfromwales.hfdesignpatterns.command.Commands.GarageDoorUpCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.LightOnCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoOffCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoOnCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoOnWithCdCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoOnWithDvdCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoOnWithRadioCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoVolumeDownCommand;
import com.wellsfromwales.hfdesignpatterns.command.Commands.StereoVolumeUpCommand;
import com.wellsfromwales.hfdesignpatterns.command.Devices.GarageDoor;
import com.wellsfromwales.hfdesignpatterns.command.Devices.Light;
import com.wellsfromwales.hfdesignpatterns.command.Devices.Stereo;


public class SimpleTest {

	public static void main(String[] args) {
		
		GarageDoor garageDoor = new GarageDoor() {
			
			@Override
			public void up() {
				System.out.println("Garage Door is Up");
			}
			
			@Override
			public void stop() {
			}
			
			@Override
			public void lightOn() {
				System.out.println("lights on!");
			}
			
			@Override
			public void lightOff() {
			}
			
			@Override
			public void down() {
			}
		};
		
		SimpleRemoteControl remote = new SimpleRemoteControl();
		Light light = new Light() {

			@Override
			public void on() {
				System.out.println("Light is On");
			}

			@Override
			public void off() {
				System.out.println("Light is Off");
			}
			
		};
		
		Stereo stereo = new Stereo() {
			int volume;

			@Override
			public void on() {
				System.out.println("Stereo is On");
			}

			@Override
			public void off() {
				System.out.println("Stereo is Off");
			}

			@Override
			public void setCd() {
				System.out.println("Stereo set to CD");
			}

			@Override
			public void setDvd() {
				System.out.println("Stereo set to DVD");
			}

			@Override
			public void setRadio() {
				System.out.println("Stereo set to Radio");
			}

			@Override
			public void setVolume(int volume) {
				this.volume = volume;
				System.out.println("Stereo volume is " + volume);
			}

			@Override
			public int getVolume() {
				return volume;
			}
			
		};
		
		LightOnCommand lightOn = new LightOnCommand(light);
		GarageDoorUpCommand gdOpen = new GarageDoorUpCommand(garageDoor);
		StereoOnCommand sOn = new StereoOnCommand(stereo);
		StereoOffCommand sOff = new StereoOffCommand(stereo);
		StereoOnWithCdCommand sOnCd = new StereoOnWithCdCommand(stereo);
		StereoOnWithDvdCommand sOnDvd = new StereoOnWithDvdCommand(stereo);
		StereoOnWithRadioCommand sOnRad = new StereoOnWithRadioCommand(stereo);
		StereoVolumeUpCommand svUp = new StereoVolumeUpCommand(stereo);
		StereoVolumeDownCommand svDown = new StereoVolumeDownCommand(stereo);
		
		remote.setCommand(lightOn);
		remote.buttonWasPressed();
		remote.setCommand(gdOpen);
		remote.buttonWasPressed();
		remote.setCommand(sOn);
		remote.buttonWasPressed();
		remote.setCommand(sOff);
		remote.buttonWasPressed();
		remote.setCommand(sOnCd);
		remote.buttonWasPressed();
		remote.setCommand(sOnDvd);
		remote.buttonWasPressed();
		remote.setCommand(sOnRad);
		remote.buttonWasPressed();
		remote.setCommand(svUp);
		remote.buttonWasPressed();
		remote.buttonWasPressed();
		remote.buttonWasPressed();
		remote.setCommand(svDown);
		remote.buttonWasPressed();
		remote.buttonWasPressed();
		remote.buttonWasPressed();
		
	}

}
