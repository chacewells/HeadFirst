package com.wellsfromwales.hfdesignpatterns.decorator;

public class HouseBlend extends Beverage {
	public HouseBlend() {
		setDescription("House Blend");
	}

	@Override
	public double cost() {
		return .89;
	}

}
