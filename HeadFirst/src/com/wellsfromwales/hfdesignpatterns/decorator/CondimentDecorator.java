package com.wellsfromwales.hfdesignpatterns.decorator;

public abstract class CondimentDecorator extends Beverage {
	@Override
	public abstract String getDescription();
}
