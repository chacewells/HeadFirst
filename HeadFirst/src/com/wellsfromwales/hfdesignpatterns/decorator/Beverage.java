package com.wellsfromwales.hfdesignpatterns.decorator;

public abstract class Beverage {
	private String description;
	
	public abstract double cost();

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
