package com.wellsfromwales.hfdesignpatterns.factory;

public class PepperoniPizza extends Pizza {
	PizzaIngredientFactory ingredientFactory;
	
	public PepperoniPizza( PizzaIngredientFactory ingredientFactory ) {
		this.ingredientFactory = ingredientFactory;
	}

	@Override
	void prepare(boolean showPrep) {
		this.showPrep = showPrep;
		System.out.println("Preparing " + name);
		dough = ingredientFactory.createDough();
		sauce = ingredientFactory.createSauce();
		cheese = ingredientFactory.createCheese();
		pepperoni = ingredientFactory.createPepperoni();
	}

}
