package com.wellsfromwales.hfdesignpatterns.factory;

public class Ingredients {
	public abstract static class Cheese {}
	public abstract static class Clams {}
	public abstract static class Dough {}
	public abstract static class Pepperoni {}
	public abstract static class Sauce {}
	public abstract static class Veggies {}
	
	public static class ReggianoCheese extends Cheese {
		@Override
		public String toString() {
			return "reggiano cheese";
		}
	}
	public static class MozzarellaCheese extends Cheese {
		@Override
		public String toString() {
			return "mozzarella cheese";
		}
	}
	
	public static class FreshClams extends Clams {
		@Override
		public String toString() {
			return "fresh clams";
		}
	}
	public static class FrozenClams extends Clams {
		@Override
		public String toString() {
			return "frozen clams";
		}
	}

	public static class ThinCrustDough extends Dough {
		@Override
		public String toString() {
			return "thin crust";
		}
	}
	public static class ThickCrustDough extends Dough {
		@Override
		public String toString() {
			return "thick crust";
		}
	}
	
	public static class SlicedPepperoni extends Pepperoni {
		@Override
		public String toString() {
			return "sliced pepperoni";
		}
	}
	
	public static class MarinaraSauce extends Sauce {
		@Override
		public String toString() {
			return "marinara sauce";
		}
	}
	public static class PlumTomatoSauce extends Sauce {
		@Override
		public String toString() {
			return "plum tomato sauce";
		}
	}

	public static class Garlic extends Veggies {
		@Override
		public String toString() {
			return "garlic";
		}
	}
	public static class Onion extends Veggies {
		@Override
		public String toString() {
			return "onion";
		}
	}
	public static class Mushroom extends Veggies {
		@Override
		public String toString() {
			return "mushroom";
		}
	}
	public static class RedPepper extends Veggies {
		@Override
		public String toString() {
			return "red pepper";
		}
	}
	public static class BlackOlives extends Veggies {
		@Override
		public String toString() {
			return "black olives";
		}
	}
	public static class Spinach extends Veggies {
		@Override
		public String toString() {
			return "spinach";
		}
	}
	public static class EggPlant extends Veggies {
		@Override
		public String toString() {
			return "eggplant";
		}
	}
}
