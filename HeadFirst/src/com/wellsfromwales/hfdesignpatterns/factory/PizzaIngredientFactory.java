package com.wellsfromwales.hfdesignpatterns.factory;

import static com.wellsfromwales.hfdesignpatterns.factory.Ingredients.*;

public interface PizzaIngredientFactory {
	
    public Dough createDough();
	public Sauce createSauce();
	public Cheese createCheese();
	public Veggies[] createVeggies();
	public Pepperoni createPepperoni();
	public Clams createClams();
	
}
