package com.wellsfromwales.hfdesignpatterns.factory;

public abstract class PizzaStore {
	
	public Pizza orderPizza(String type, boolean showPrep) {
		Pizza pizza = createPizza(type);
		
		pizza.prepare(showPrep);
		pizza.bake();
		pizza.cut();
		pizza.box();
		
		return pizza;
	}
	
	protected abstract Pizza createPizza(String type);
	
}
