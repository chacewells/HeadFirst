package com.wellsfromwales.hfdesignpatterns.factory;

public class Test {

	public static void main(String[] args) {
		PizzaStore nyPizzaStore = new NYPizzaStore();
		PizzaStore chicagoPizzaStore = new ChicagoPizzaStore();
		
		String[] pizzaTypes = {"cheese","clam","pepperoni","veggie"};
		for (String type : pizzaTypes) {
			Pizza nyPizza = nyPizzaStore.orderPizza(type, false);
			Pizza chicagoPizza = chicagoPizzaStore.orderPizza(type, false);
			
			System.out.println(nyPizza);
			System.out.println(chicagoPizza);
		}
	}

}
