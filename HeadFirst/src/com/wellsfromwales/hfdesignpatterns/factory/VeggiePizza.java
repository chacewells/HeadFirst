package com.wellsfromwales.hfdesignpatterns.factory;

public class VeggiePizza extends Pizza {
	
	PizzaIngredientFactory ingredientFactory;
	
	public VeggiePizza( PizzaIngredientFactory ingredientFactory) {
		this.ingredientFactory = ingredientFactory;
	}

	@Override
	void prepare(boolean showPrep) {
		this.showPrep = showPrep;
		System.out.println( "Preparing " + name);
		dough = ingredientFactory.createDough();
		sauce = ingredientFactory.createSauce();
		cheese = ingredientFactory.createCheese();
		veggies = ingredientFactory.createVeggies();
	}

}
