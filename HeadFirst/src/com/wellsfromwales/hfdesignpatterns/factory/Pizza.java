package com.wellsfromwales.hfdesignpatterns.factory;

import java.util.Arrays;

import static com.wellsfromwales.hfdesignpatterns.factory.Ingredients.*;

public abstract class Pizza {
	String name;
	protected boolean showPrep;
	
	Dough dough;
	Sauce sauce;
	Veggies veggies[];
	Pepperoni pepperoni;
	Clams clam;
	Cheese cheese;
	
	abstract void prepare(boolean showPrep);
	
	void bake() {
		if (showPrep)
			System.out.println("Bake for 25 minutes at 350");
	}
	
	void cut() {
		if (showPrep)
			System.out.println("Cutting pizza into diagonal slices");
	}
	
	void box() {
		if (showPrep)
			System.out.println("place pizza in official PizzaStore box");
	}
	
	void setName(String name) {
		this.name = name;
	}
	
	String getName() {
		return name;
	}

	public Cheese getCheese() {
		return cheese;
	}

	public void setCheese(Cheese cheese) {
		this.cheese = cheese;
	}

	@Override
	public String toString() {
		return "Pizza [name=" + name + ", dough="
				+ dough + ", sauce=" + sauce + ", veggies="
				+ Arrays.toString(veggies) + ", pepperoni=" + pepperoni
				+ ", clam=" + clam + ", cheese=" + cheese + "]";
	}

}
